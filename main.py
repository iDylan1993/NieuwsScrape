import pymysql
from flask import Flask,abort
from flask_sqlalchemy import SQLAlchemy

import csv
from io import StringIO

SQL_SERVER = "127.0.0.1"
SQL_USER = "nieuws"
SQL_PASS = "scraper"
SQL_DB = "nieuwsscraper"


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://' + SQL_USER + ":" + SQL_PASS + "@" + SQL_SERVER + "/" + SQL_DB
db = SQLAlchemy(app)

class CsvRow(db.Model):
    __tablename__ = "csvdata"
    id = db.Column(db.Integer, primary_key = True)
    origin = db.Column(db.String(150), unique = False, nullable = True)
    timestamp = db.Column(db.String(150), unique = False, nullable = True)
    content = db.Column(db.String(8000), unique = False, nullable = True)
    title = db.Column(db.String(150), unique = False, nullable = True)
    comment_count = db.Column(db.String(150), unique = False, nullable = True)
    retweet_count = db.Column(db.String(150), unique = False, nullable = True)

    def __repr__(self):
        return "<{klass} @{id:x} {attrs}>".format(
            klass=self.__class__.__name__,
            id=id(self) & 0xFFFFFFFF,
            attrs=" ".join("{}={!r}".format(k, v) for k, v in self.__dict__.items()))


def read_csv(file):
    reader = csv.DictReader(file, fieldnames=("origin","timestamp","content","title","comment_count","retweet_count"), delimiter=",")
    for row in reader:
        #print("ROW %s" % row)
        r_origin = row["origin"]
        r_timestamp = row["timestamp"]
        r_content = row["content"]
        r_title = row["title"]
        r_comment_count = row["comment_count"]
        r_retweet_count = row["retweet_count"]
        
        """
        print("ORIGIN = %s" % r_origin)
        print("TIMESTAMP = %s" % r_timestamp)
        print("CONTENT = %s" % r_content)
        print("TITLE = %s" % r_title)
        print("COMMENT_COUNT = %s" % r_comment_count)
        print("RETWEET_COUNT = %s" % r_retweet_count)
        """

        csvrow = CsvRow(origin = r_origin, timestamp = r_timestamp, content = r_content, title = r_title, comment_count = r_comment_count, retweet_count = r_retweet_count)
        print("generated csvrow '%s'" % csvrow)
        db.session.add(csvrow)

@app.route("/")
def hello():
    return "Hello World!"

def start():
    db.create_all()

start()

"""
i = CsvRow(origin = "twitter", timestamp = "00000", title = "title", comment_count = "1", retweet_count = "1")

db.session.add(i)
db.session.commit()
"""
with open("data.csv", "r") as f:
    read_csv(f)
    db.session.commit()
